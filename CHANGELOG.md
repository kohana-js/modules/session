# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [2.0.8](https://gitlab.com/kohana-js/modules/session/compare/v2.0.7...v2.0.8) (2022-09-07)

### [2.0.7](https://gitlab.com/kohana-js/modules/session/compare/v2.0.6...v2.0.7) (2022-03-09)

### [2.0.6](https://gitlab.com/kohana-js/modules/session/compare/v2.0.5...v2.0.6) (2021-12-20)

### [2.0.5](https://gitlab.com/kohana-js/modules/session/compare/v2.0.4...v2.0.5) (2021-12-20)


### Bug Fixes

* test error ([1e0853e](https://gitlab.com/kohana-js/modules/session/commit/1e0853e74c932282a83ed8e98b8c99d51915df35))

### 2.0.4 (2021-12-20)


### Bug Fixes

* dependency ([a3368a4](https://gitlab.com/kohana-js/modules/session/commit/a3368a43ff1f3f04531d500d197eff5d0a57050a))

## [2.0.3] - 2021-10-16
### Fixed
- fix controller mixin redirect in setup will have error on session mixin after();

## [2.0.2] - 2021-09-07
### Added
- create CHANGELOG.md