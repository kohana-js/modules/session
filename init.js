const { KohanaJS } = require('kohanajs');

KohanaJS.initConfig(new Map([
  ['cookie', require('./config/cookie')],
  ['session', require('./config/session')],
]));
